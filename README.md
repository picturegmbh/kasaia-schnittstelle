# Kasaia-Schnittstelle

Die Klasse Schnittstelle stellt eine Methode zum Laden der Vorschaudaten
für alle in der "KGSt-Datenbank Stellenbewertung" vorhandenen freigegebenen Stellen bereit.

## Methoden der Schnittstellen
**lade_Stellendatenbankeinträge**() -> List\<Stellendatenbankeintrag\>

liefert eine Liste mit den Vorschaudaten aller freigegebenen Stellen

## Datenstrukturen
Die Klasse **Stellendatenbankeintrag** enthält die Felder welche im KGSt-Portal für die Darstellung der 
Vorschau-Informationen benötigt werden sowie zusätzlich alle Daten, die benötigt werden, um die übermittelten 
Stellen im KGSt-Portal den "Portal-Themen" zuordnen zu können. 

Hinweise, welches Feld welcher Spalte aus der Datenbank v1 entspricht, sind als JavaDoc an den Feldern hinterlegt.

Zu jeder der übermittelten Stellen kann in der Stellenbewertungsdatenbank jeweils entweder  
- (nur) eine Beamtenbewertung oder
- (nur) eine Tarifbewertung oder
- eine Beamtenbewertung UND eine Tarifbewertung 

vorliegen. In Abhängigkeit von den vorliegenden Bewertungen sind die entsprechenden Felder in den von der Schnittstelle 
zurückgelieferten Antwort-Datensätzen teilweise leer.

## Hinweise zur Taktung der Schnittstellenaufrufe
Die Stellen-Liste sollte täglich komplett neu ausgelesen werden, da es in der Stellen-Datenbank möglich ist, die 
Freigabe einer Stelle  zurückzuziehen, die Freigabe durch eine ältere Version der Stelle zu ersetzen und auch eine 
Stelle zu löschen.

## Hinweise zur Einrichtung des Projektes

Vorausgesetzt wird, dass bereits eine Entwicklungsumgebung (z.B. IntelliJ ooder Eclipse) inkl. Maven (Version 3.5.3 
oder neuer) sowie ein Java Development Kit (JDK) in Version 8 (oder neuer) installiert sind. 

Die Klasse **"Kasaia-Schnittstelle"** ist mit den auf der Seite
https://confluence.neusta-ws.de/display/KGST/Konzeption+zur+Integration+der+KGSt-Stellendatenbank+v2+in+die+%28globale%29+Suchfunktion+des+KGSt-Portals
hinterlegten Parametern basis_Url und token zu instanziieren.

