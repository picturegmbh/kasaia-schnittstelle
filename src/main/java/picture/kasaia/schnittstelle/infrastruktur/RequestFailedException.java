package picture.kasaia.schnittstelle.infrastruktur;

public class RequestFailedException extends RuntimeException{

    public RequestFailedException(){
    }

    public RequestFailedException(String message){
        super(message);
    }
}
