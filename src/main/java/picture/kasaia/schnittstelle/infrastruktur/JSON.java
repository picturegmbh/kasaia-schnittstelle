package picture.kasaia.schnittstelle.infrastruktur;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;
import static com.fasterxml.jackson.databind.DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_USING_DEFAULT_VALUE;

public class JSON {
    private static ObjectMapper mapper = new ObjectMapper().enable(READ_UNKNOWN_ENUM_VALUES_USING_DEFAULT_VALUE)
            .disable(FAIL_ON_UNKNOWN_PROPERTIES);

    public static String als_String(Object object){
        if(object == null){
            return null;
        }
        try{
            return mapper.writeValueAsString(object);
        } catch(Exception checkedException){
            throw new RuntimeException(checkedException);
        }
    }

    public static <T> T einlesen(String json, Class<T> type){
        try{
            return mapper.readValue(json, type);
        } catch(Exception ex){
            throw new RuntimeException(ex);
        }
    }

    public static <T> T einlesen(Path file, Class<T> type){
        try{
            return mapper.readValue(new String(Files.readAllBytes(file), StandardCharsets.UTF_8), type);
        } catch(Exception ex){
            throw new RuntimeException(ex);
        }
    }

    public static <T> List<T> Liste_einlesen(String json, Class<T> type){
        if(json == null){
            return new ArrayList<>();
        }
        try{
            return mapper.readValue(json, mapper.getTypeFactory().constructCollectionType(List.class, type));
        } catch(Exception ex){
            throw new RuntimeException(ex);
        }
    }

    public static <T> T Liste_einlesen(Path file, Class<T> type){
        try{
            return mapper.readValue(new String(Files.readAllBytes(file), StandardCharsets.UTF_8),
                    mapper.getTypeFactory().constructCollectionType(List.class, type));
        } catch(Exception ex){
            throw new RuntimeException(ex);
        }
    }

    public static <K, V> Map<K, V> Map_einlesen(String json, Class<K> key, Class<V> value){
        if(json == null){
            return new HashMap<>();
        }
        try{
            return mapper.readValue(json, mapper.getTypeFactory().constructMapType(Map.class, key, value));
        } catch(Exception ex){
            throw new RuntimeException(ex);
        }
    }
}
