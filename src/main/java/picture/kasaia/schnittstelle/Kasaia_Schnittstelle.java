package picture.kasaia.schnittstelle;

import picture.kasaia.schnittstelle.infrastruktur.JSON;
import picture.kasaia.schnittstelle.infrastruktur.RequestFailedException;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;
import java.io.Closeable;
import java.util.List;

public class Kasaia_Schnittstelle implements Closeable {
    private Client client;
    private String baseUrl;
    private String token;

    /**
     * instanziiert die Kasaia-Schnittstelle zum Abruf der Daten für den Aufbau des Suchindex im KGSt®-Portal aus
     * der KGSt®-Datenbank Stellenbewertung 2.0
     * @param baseUrl Host auf dem die Schnittstelle erreichbar ist - für die Produktivumgebung https://datenbank-stellenbewertung.kgst.de/kasaia
     * @param token "Passwort" für den Schnittstellenzugriff hinterlegt auf
     *              https://confluence.neusta-ws.de/display/KGST/Konzeption+zur+Integration+der+KGSt-Stellendatenbank+v2+in+die+%28globale%29+Suchfunktion+des+KGSt-Portals
     */
    public Kasaia_Schnittstelle(String baseUrl, String token) {
        this.baseUrl = baseUrl;
        this.token = token;
        client = ClientBuilder.newBuilder().build();
    }


    public List<Stellendatenbankeintrag> lade_Stellendatenbankeinträge() {
        Response response = null;
        try {
            response = client.target(baseUrl + "/rest/stellenbewertungen?token=" + token).request().get();
            throwErrors(response);
            return JSON.Liste_einlesen(response.readEntity(String.class), Stellendatenbankeintrag.class);
        } finally {
            closeEntity(response);
        }
    }


    private void throwErrors(Response response) {
        if (response.getStatus() < 200 || response.getStatus() > 299) {
            throw new RequestFailedException(response.readEntity(String.class));
        }
    }

    private void closeEntity(Response response) {
        if (response != null) {
            response.close();
        }
    }

    @Override
    public void close() {
        try {
            if (client != null) {
                client.close();
            }
            this.client = null;
        } catch (Exception ignore) {
        }
    }
}
