package picture.kasaia.schnittstelle;

import java.util.Date;

public class Stellendatenbankeintrag {

    /** entspricht stellenbewertungId aus v1 */
    public long fortlaufende_Nummer;

    /** entspricht title aus v1 */
    public String Bezeichnung;

    /** entspricht modified aus v1 */
    public Date Änderungsdatum;

    /** sollte sizeRange aus v1 entsprechen */
    public String Größenklasse;

    /** entspricht pad aus v 1*/
    public String PAD_Ziffer;

    /** entspricht number_ aus v1*/
    public String Organisationsziffer;

    /** entspricht description aus v1 */
    public String Einleitung;

    /** entspricht grade aus v1, ist null wenn nur eine Tarifbewertung vorliegt */
    public String Besoldungsgruppe;

    /** Url zum Öffnen der Beamtenbewertung der Stelle in der Datenbank-Stellenbewertung 2.0,
     *  ist null wenn nur eine Tarifbewertung vorliegt
     **/
    public String Beamtenbewertung_Url;

    /** analog zu Besoldungsgruppe, z.B. EG 9 oder EG S11a, ist null wenn nur eine Beamtenbewertung vorliegt */
    public String Entgeltgruppe;

    /** Url zum Öffnen der Tarifbewertung der Stelle in der Datenbank-Stellenbewertung 2.0,
     * ist null wenn nur eine Beamtenbewertung vorliegt */
    public String Tarifbewertung_Url;
}
